#! /usr/bin/python

import os.path
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)

GPIO.output(18, GPIO.HIGH)
GPIO.output(23, GPIO.HIGH)
GPIO.output(24, GPIO.HIGH)
GPIO.output(25, GPIO.HIGH)
GPIO.output(8, GPIO.HIGH)
GPIO.output(7, GPIO.HIGH)
GPIO.output(12, GPIO.HIGH)
#Tornado Folder Paths
settings = dict(
	template_path = os.path.join(os.path.dirname(__file__), "templates"),
	static_path = os.path.join(os.path.dirname(__file__), "static")
	)

#Tonado server port
PORT = 80


class MainHandler(tornado.web.RequestHandler):
  def get(self):
     print("[HTTP](MainHandler) User Connected.")
     self.render("index.html")

	
class WSHandler(tornado.websocket.WebSocketHandler):
  def open(self):
    print('[WS] Connection was opened.')
 
  def on_message(self, message):
    print('[WS] Incoming message:', message)
    if message=='t1down':
      f = open('flag.txt','w')
      f.write('M')
      f.close()
      GPIO.output(18, GPIO.LOW)
    if message=='t1up':
      GPIO.output(18, GPIO.HIGH)
    if message=='t2down':
      f = open('flag.txt','w')
      f.write('M')
      f.close()
      GPIO.output(23, GPIO.LOW)
    if message=='t2up':
      GPIO.output(23, GPIO.HIGH)
    if message=='t3down':
      f = open('flag.txt','w')
      f.write('M')
      f.close()
      GPIO.output(24, GPIO.LOW)
    if message=='t3up':
      GPIO.output(24, GPIO.HIGH);
    if message=='t4down':
      f = open('flag.txt','w')
      f.write('M')
      f.close()
      GPIO.output(25, GPIO.LOW)
    if message=='t4up':
      GPIO.output(25, GPIO.HIGH);
    if message=='t5down':
      f = open('flag.txt','w')
      f.write('M')
      f.close()
      GPIO.output(8, GPIO.LOW)
    if message=='t5up':
      GPIO.output(8, GPIO.HIGH);
    if message=='t6down':
      f = open('flag.txt','w')
      f.write('M')
      f.close()
      GPIO.output(7, GPIO.LOW)
    if message=='t6up':
      GPIO.output(7, GPIO.HIGH);
    if message=='t7down':
      f = open('flag.txt','w')
      f.write('M')
      f.close()
      GPIO.output(12, GPIO.LOW)
    if message=='t7up':
      GPIO.output(12, GPIO.HIGH);

    if message=='pola1':
      f = open('flag.txt','w')
      f.write('1')
      f.close()
    if message=='pola2':
      f = open('flag.txt','w')
      f.write('2')
      f.close()
    if message=='pola3':
      f = open('flag.txt','w')
      f.write('3')
      f.close()
    if message=='pola4':
      f = open('flag.txt','w')
      f.write('4')
      f.close()
    if message=='pola5':
      f = open('flag.txt','w')
      f.write('5')
      f.close()
    if message=='pola6':
      f = open('flag.txt','w')
      f.write('6')
      f.close()
    if message=='pola7':
      f = open('flag.txt','w')
      f.write('7')
      f.close()
    if message=='l1a':
      f = open('flag.txt','w')
      f.write('q')
      f.close()
    if message=='l1b':
      f = open('flag.txt','w')
      f.write('w')
      f.close()
    if message=='l1c':
      f = open('flag.txt','w')
      f.write('e')
      f.close()
    if message=='l1d':
      f = open('flag.txt','w')
      f.write('r')
      f.close()
    if message=='l2a':
      f = open('flag.txt','w')
      f.write('a')
      f.close()
    if message=='l2b':
      f = open('flag.txt','w')
      f.write('s')
      f.close()
    if message=='l2c':
      f = open('flag.txt','w')
      f.write('d')
      f.close()
    if message=='l2d':
      f = open('flag.txt','w')
      f.write('f')
      f.close()
    if message=='l3a':
      f = open('flag.txt','w')
      f.write('z')
      f.close()
    if message=='l3b':
      f = open('flag.txt','w')
      f.write('x')
      f.close()
    if message=='l3c':
      f = open('flag.txt','w')
      f.write('c')
      f.close()
    if message=='l3d':
      f = open('flag.txt','w')
      f.write('v')
      f.close()
    if message=='l4a':
      f = open('flag.txt','w')
      f.write('t')
      f.close()
    if message=='l4b':
      f = open('flag.txt','w')
      f.write('y')
      f.close()
    if message=='l4c':
      f = open('flag.txt','w')
      f.write('u')
      f.close()
    if message=='l4d':
      f = open('flag.txt','w')
      f.write('i')
      f.close()
  def on_close(self):
    print('[WS] Connection was closed.')


application = tornado.web.Application([
  (r'/', MainHandler),
  (r'/ws', WSHandler),
  ], **settings)


if __name__ == "__main__":
    try:
        http_server = tornado.httpserver.HTTPServer(application)
        http_server.listen(PORT)
        main_loop = tornado.ioloop.IOLoop.instance()

        print("Tornado Server started")
        main_loop.start()

    except:
        print("Exception triggered - Tornado Server stopped.")
        #GPIO.cleanup()

#End of Program
