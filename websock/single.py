import RPi.GPIO as GPIO
import time
import sys

data = int(sys.argv[1])
npwm = int(sys.argv[2])
GPIO.setmode(GPIO.BCM)
GPIO.setup(data, GPIO.OUT)
pwm = GPIO.PWM(data,900)
pwm.start(npwm)

while True:
	time.sleep(1)
