import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)

GPIO.output(18, GPIO.HIGH)
GPIO.output(23, GPIO.HIGH)
GPIO.output(24, GPIO.HIGH)
GPIO.output(25, GPIO.HIGH)
GPIO.output(8, GPIO.HIGH)
GPIO.output(7, GPIO.HIGH)
GPIO.output(12, GPIO.HIGH)

pwm1 = GPIO.PWM(18,900)
pwm2 = GPIO.PWM(23, 900)
pwm3 = GPIO.PWM(24,900)
pwm4 = GPIO.PWM(25, 900)
onMode = ''
def singleOn(num):
	#GPIO.output(18, GPIO.HIGH)
	#GPIO.output(23, GPIO.HIGH)
	#GPIO.output(24, GPIO.HIGH)
	#GPIO.output(25, GPIO.HIGH)
	#GPIO.output(8, GPIO.HIGH)
	#GPIO.output(7, GPIO.HIGH)
	#GPIO.output(12, GPIO.HIGH)
	GPIO.output(num, GPIO.LOW)
def allOff():
	GPIO.output(18, GPIO.HIGH)
	GPIO.output(23, GPIO.HIGH)
	GPIO.output(24, GPIO.HIGH)
	GPIO.output(25, GPIO.HIGH)
	GPIO.output(8, GPIO.HIGH)
	GPIO.output(7, GPIO.HIGH)
	GPIO.output(12, GPIO.HIGH)
def digitSetup():
	global pwm1
	global pwm2
	global pwm3
	global pwm4
	pwm1.stop()
	pwm2.stop()
	pwm3.stop()
	pwm4.stop()
	GPIO.setup(18, GPIO.OUT)
	GPIO.setup(23, GPIO.OUT)
	GPIO.setup(24, GPIO.OUT)
	GPIO.setup(25, GPIO.OUT)
	GPIO.setup(8, GPIO.OUT)
	GPIO.setup(7, GPIO.OUT)
	GPIO.setup(12, GPIO.OUT)

	GPIO.output(18, GPIO.HIGH)
	GPIO.output(23, GPIO.HIGH)
	GPIO.output(24, GPIO.HIGH)
	GPIO.output(25, GPIO.HIGH)
	GPIO.output(8, GPIO.HIGH)
	GPIO.output(7, GPIO.HIGH)
	GPIO.output(12, GPIO.HIGH)
def manualOn(ngpio):
	global onMode
	if onMode=='pwm':
		digitSetup()
	onMode='manual'
	#GPIO.output(ngpio, GPIO.LOW)
def manualOff(ngpio):
	global onMode
	if onMode=='pwm':
		digitSetup()
	onMode='manual'
	#GPIO.output(ngpio, GPIO.HIGH)
def polaOn():
	global onMode
	if onMode=='pwm':
		digitSetup()
	onMode='pola'

while True:
	f = open('flag.txt','r')
	data = f.read(1)
	f.close()
	allOff()
	if data=='A':
		manualOn(18)
	if data=='Z':
		manualOff(18)
	if data=='S':
		manualOn(23)
	if data=='X':
		manualOff(23)
	if data=='D':
		manualOn(24)
	if data=='C':
		manualOff(24)
	if data=='F':
		manualOn(25)
	if data=='V':
		manualOff(25)
	if data=='G':
		manualOn(8)
	if data=='B':
		manualOff(8)
	if data=='H':
		manualOn(7)
	if data=='N':
		manualOff(7)
	if data=='J':
		manualOn(12)
	if data=='M':
		manualOff(12)
	
	#if data=='':
	#	applyPWM()
	
	if data=='1':
		polaOn()
		onMode = 'pola'
		gpiolist = [18,23,24,25,8,7,12]
		for val in gpiolist:
			singleOn(val)
			time.sleep(.5)
	if data=='2':
		polaOn()
		onMode = 'pola'
		gpiolist = [12,7,8,25,24,23,18]
		for val in gpiolist:
			singleOn(val)
			time.sleep(.5)
	if data=='3':
		polaOn()
		onMode = 'pola'
		gpiolist = [12,18,7,23,8,24,25]
		for val in gpiolist:
			singleOn(val)
			time.sleep(.5)
	if data=='4':
		polaOn()
		onMode = 'pola'
		gpiolist = [25,24,8,23,7,18,12]
		for val in gpiolist:
			singleOn(val)
			time.sleep(.5)
	if data=='5':
		polaOn()
		onMode = 'pola'
		gpiolist = [24,8,25,7,23,12,18]
		for val in gpiolist:
			singleOn(val)
			time.sleep(.5)
	if data=='6':
		polaOn()
		onMode = 'pola'
		gpiolist = [7,25,18,23,12,8,24]
		for val in gpiolist:
			singleOn(val)
			time.sleep(.5)
	if data=='7':
		polaOn()
		onMode = 'pola'
		gpiolist = [8,23,7,24,18,12,25]
		for val in gpiolist:
			singleOn(val)
			time.sleep(.5)
	if onMode=='pola':
		time.sleep(1)
