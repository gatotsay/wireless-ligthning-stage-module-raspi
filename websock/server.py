#! /usr/bin/python

import os.path
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import RPi.GPIO as GPIO
import thread
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)

GPIO.output(18, GPIO.HIGH)
GPIO.output(23, GPIO.HIGH)
GPIO.output(24, GPIO.HIGH)
GPIO.output(25, GPIO.HIGH)
GPIO.output(8, GPIO.HIGH)
GPIO.output(7, GPIO.HIGH)
GPIO.output(12, GPIO.HIGH)

pwm = GPIO.PWM(18,900)
pwm2 = GPIO.PWM(23, 900)
pwm3 = GPIO.PWM(24,900)
pwm4 = GPIO.PWM(25, 900)

#Tornado Folder Paths
settings = dict(
	template_path = os.path.join(os.path.dirname(__file__), "templates"),
	static_path = os.path.join(os.path.dirname(__file__), "static")
	)

#Tonado server port
PORT = 80

def pwmMode():
	global pwm,pwm2,pwm3,pwm4
	global onMode
	pwm.start(100)
	pwm2.start(100)
	pwm3.start(100)
	pwm4.start(100)
	allOff()
	onMode='pwm'
def digitalMode():
	global onMode
	global pwm,pwm2,pwm3,pwm4
	if onMode=='pwm':
		pwm.stop()
		pwm2.stop()
		pwm3.stop()
		pwm4.stop()
def singleOn(num):
	GPIO.output(num, GPIO.LOW)
def allOff():
	GPIO.output(18, GPIO.HIGH)
	GPIO.output(23, GPIO.HIGH)
	GPIO.output(24, GPIO.HIGH)
	GPIO.output(25, GPIO.HIGH)
	GPIO.output(8, GPIO.HIGH)
	GPIO.output(7, GPIO.HIGH)
	GPIO.output(12, GPIO.HIGH)

onMode = ''
polaList = [18,23,24,25,8,7]
def mThread(threadName):
	global onMode
	global polaList
	while onMode=='pola':
		allOff()
		for val in polaList:
			singleOn(val)
			time.sleep(.5)
	

class MainHandler(tornado.web.RequestHandler):
  def get(self):
     print("[HTTP](MainHandler) User Connected.")
     self.render("index.html")

	
class WSHandler(tornado.websocket.WebSocketHandler):
  def open(self):
    print('[WS] Connection was opened.')
  def on_message(self, message):
    global polaList
    global onMode
    global pwm,pwm2,pwm3,pwm4	
    print('[WS] Incoming message:', message)
    if message=='t1down':
      digitalMode()
      GPIO.output(18, GPIO.LOW)
      onMode='manual'
    if message=='t1up':
      GPIO.output(18, GPIO.HIGH)
    if message=='t2down':
      digitalMode()
      GPIO.output(23, GPIO.LOW)
      onMode='manual'
    if message=='t2up':
      GPIO.output(23, GPIO.HIGH)
    if message=='t3down':
      digitalMode()
      GPIO.output(24, GPIO.LOW)
      onMode='manual'
    if message=='t3up':
      GPIO.output(24, GPIO.HIGH)
    if message=='t4down':
      digitalMode()
      GPIO.output(25, GPIO.LOW)
      onMode='manual'
    if message=='t4up':
      GPIO.output(25, GPIO.HIGH)
    if message=='t5down':
      digitalMode()
      GPIO.output(8, GPIO.LOW)
      onMode='manual'
    if message=='t5up':
      GPIO.output(8, GPIO.HIGH)
    if message=='t6down':
      digitalMode()
      GPIO.output(7, GPIO.LOW)
      onMode='manual'
    if message=='t6up':
      GPIO.output(7, GPIO.HIGH)
    if message=='t7down':
      digitalMode()
      GPIO.output(12, GPIO.LOW)
      onMode='manual'
    if message=='t7up':
      GPIO.output(12, GPIO.HIGH)
    if message=='pola1':
      digitalMode()
      polaList=[18,23,24,25,8,7]
      if onMode!='pola':
        thread.start_new_thread(mThread,('thread1',))
      onMode='pola';
    if message=='pola2':
      digitalMode()
      polaList=[7,8,25,24,23,18]
      if onMode!='pola':
        thread.start_new_thread(mThread,('thread1',))
      onMode='pola';
    if message=='pola3':
      digitalMode()
      polaList=[18,7,23,8,24,25]
      if onMode!='pola':
        thread.start_new_thread(mThread,('thread1',))
      onMode='pola';
    if message=='pola4':
      digitalMode()
      polaList=[25,24,8,23,7,18]
      if onMode!='pola':
        thread.start_new_thread(mThread,('thread1',))
      onMode='pola';
    if message=='pola5':
      digitalMode()
      polaList=[24,8,25,7,23,18]
      if onMode!='pola':
        thread.start_new_thread(mThread,('thread1',))
      onMode='pola';
    if message=='pola6':
      digitalMode()
      polaList=[7,25,18,23,8,24]
      if onMode!='pola':
        thread.start_new_thread(mThread,('thread1',))
      onMode='pola';
    if message=='l1a':
      if onMode!='pwm':
        pwmMode()
      pwm.ChangeDutyCycle(0)
    if message=='l1b':
      if onMode!='pwm':
        pwmMode()
      pwm.ChangeDutyCycle(20)
    if message=='l1c':
      if onMode!='pwm':
        pwmMode()
      pwm.ChangeDutyCycle(50)
    if message=='l1d':
      if onMode!='pwm':
        pwmMode()
      pwm.ChangeDutyCycle(100)
    if message=='l2a':
      if onMode!='pwm':
        pwmMode()
      pwm2.ChangeDutyCycle(0)
    if message=='l2b':
      if onMode!='pwm':
        pwmMode()
      pwm2.ChangeDutyCycle(20)
    if message=='l2c':
      if onMode!='pwm':
        pwmMode()
      pwm2.ChangeDutyCycle(50)
    if message=='l2d':
      if onMode!='pwm':
        pwmMode()
      pwm2.ChangeDutyCycle(100)
    if message=='l3a':
      if onMode!='pwm':
        pwmMode()
      pwm3.ChangeDutyCycle(0)
    if message=='l3b':
      if onMode!='pwm':
        pwmMode()
      pwm3.ChangeDutyCycle(20)
    if message=='l3c':
      if onMode!='pwm':
        pwmMode()
      pwm3.ChangeDutyCycle(50)
    if message=='l3d':
      if onMode!='pwm':
        pwmMode()
      pwm3.ChangeDutyCycle(100)
    if message=='l4a':
      if onMode!='pwm':
        pwmMode()
      pwm4.ChangeDutyCycle(0)
    if message=='l4b':
      if onMode!='pwm':
        pwmMode()
      pwm4.ChangeDutyCycle(20)
    if message=='l4c':
      if onMode!='pwm':
        pwmMode()
      pwm4.ChangeDutyCycle(50)
    if message=='l4d':
      if onMode!='pwm':
        pwmMode()
      pwm4.ChangeDutyCycle(100)
  def on_close(self):
    print('[WS] Connection was closed.')


application = tornado.web.Application([
  (r'/', MainHandler),
  (r'/ws', WSHandler),
  ], **settings)


if __name__ == "__main__":
    try:
        http_server = tornado.httpserver.HTTPServer(application)
        http_server.listen(PORT)
        main_loop = tornado.ioloop.IOLoop.instance()

        print("Tornado Server started")
        main_loop.start()

    except:
        print("Exception triggered - Tornado Server stopped.")
        #GPIO.cleanup()

#End of Program
