$(document).ready(function(){

        var WEBSOCKET_ROUTE = "/ws";
		var isTouch = 'ontouchstart' in document.documentElement;

        if(window.location.protocol == "http:"){
            //localhost
            var ws = new WebSocket("ws://" + window.location.host + WEBSOCKET_ROUTE);
            }
        else if(window.location.protocol == "https:"){
            //Dataplicity
            var ws = new WebSocket("wss://" + window.location.host + WEBSOCKET_ROUTE);
            }

        ws.onopen = function(evt) {
            $("#ws-status").html("Connected");
            };

        ws.onmessage = function(evt) {
            };

        ws.onclose = function(evt) {
            $("#ws-status").html("Disconnected");
            };

			
	    	$('#keytuts').keydown(function(e){
			  $('#keytuts').val('');
			  if(e.keyCode==49){
				ws.send("t1down");
			  }
			  if(e.keyCode==50){
				ws.send("t2down");
			  }
			  if(e.keyCode==51){
				ws.send("t3down");
			  }
			  if(e.keyCode==52){
				ws.send("t4down");
			  }
			  if(e.keyCode==53){
				ws.send("t5down");
			  }
			  if(e.keyCode==54){
				ws.send("t6down");
			  }
			  console.log(e.keyCode);
			});
			$('#keytuts').keyup(function(e){
			  $('#keytuts').val('');
			  if(e.keyCode==49){
				ws.send("t1up");
			  }
			  if(e.keyCode==50){
				ws.send("t2up");
			  }
			  if(e.keyCode==51){
				ws.send("t3up");
			  }
			  if(e.keyCode==52){
				ws.send("t4up");
			  }
			  if(e.keyCode==53){
				ws.send("t5up");
			  }
			  if(e.keyCode==54){
				ws.send("t6up");
			  }
			  console.log(e.keyCode);
			});
			
			
			
			
			
        $("#tuts1").mousedown(function(){
            ws.send("t1down");
            });
		$("#tuts1").mouseup(function(){
            ws.send("t1up");
            });
		$("#tuts1").on('touchstart', function(){
			if(isTouch){
				ws.send("t1down");
			}
		});
		$("#tuts1").on('touchend', function(){
			if(isTouch){
				ws.send("t1up");
			}
		});
		
		
		$("#tuts2").mousedown(function(){
            ws.send("t2down");
            });
		$("#tuts2").mouseup(function(){
            ws.send("t2up");
            });
		$("#tuts2").on('touchstart', function(){
			if(isTouch){
				ws.send("t2down");
			}
		});
		$("#tuts2").on('touchend', function(){
			if(isTouch){
				ws.send("t2up");
			}
		});
		
		
		$("#tuts3").mousedown(function(){
            ws.send("t3down");
            });
		$("#tuts3").mouseup(function(){
            ws.send("t3up");
            });
		$("#tuts3").on('touchstart', function(){
			if(isTouch){
				ws.send("t3down");
			}
		});
		$("#tuts3").on('touchend', function(){
			if(isTouch){
				ws.send("t3up");
			}
		});

        
		$("#tuts4").mousedown(function(){
            ws.send("t4down");
            });
		$("#tuts4").mouseup(function(){
            ws.send("t4up");
            });
		$("#tuts4").on('touchstart', function(){
			if(isTouch){
				ws.send("t4down");
			}
		});
		$("#tuts4").on('touchend', function(){
			if(isTouch){
				ws.send("t4up");
			}
		});
		
		$("#tuts5").mousedown(function(){
            ws.send("t5down");
            });
		$("#tuts5").mouseup(function(){
            ws.send("t5up");
            });
		$("#tuts5").on('touchstart', function(){
			if(isTouch){
				ws.send("t5down");
			}
		});
		$("#tuts5").on('touchend', function(){
			if(isTouch){
				ws.send("t5up");
			}
		});
		
		$("#tuts6").mousedown(function(){
            ws.send("t6down");
            });
		$("#tuts6").mouseup(function(){
            ws.send("t6up");
            });
		$("#tuts6").on('touchstart', function(){
			if(isTouch){
				ws.send("t6down");
			}
		});
		$("#tuts6").on('touchend', function(){
			if(isTouch){
				ws.send("t6up");
			}
		});
		
		$("#tuts7").mousedown(function(){
            ws.send("t7down");
            });
		$("#tuts7").mouseup(function(){
            ws.send("t7up");
            });
		$("#tuts7").on('touchstart', function(){
			if(isTouch){
				ws.send("t7down");
			}
		});
		$("#tuts7").on('touchend', function(){
			if(isTouch){
				ws.send("t7up");
			}
		});
		
		$("#pola1").mousedown(function(){
            ws.send("pola1");
        });
		$("#pola1").on('touchstart', function(){
			if(isTouch){
				ws.send("pola1");
			}
		});
		
		$("#pola2").mousedown(function(){
            ws.send("pola2");
        });
		$("#pola2").on('touchstart', function(){
			if(isTouch){
				ws.send("pola2");
			}
		});
		
		$("#pola3").mousedown(function(){
            ws.send("pola3");
        });
		$("#pola3").on('touchstart', function(){
			if(isTouch){
				ws.send("pola3");
			}
		});
		
		$("#pola4").mousedown(function(){
            ws.send("pola4");
        });
		$("#pola4").on('touchstart', function(){
			if(isTouch){
				ws.send("pola4");
			}
		});
		
		$("#pola5").mousedown(function(){
            ws.send("pola5");
        });
		$("#pola5").on('touchstart', function(){
			if(isTouch){
				ws.send("pola5");
			}
		});
		
		$("#pola6").mousedown(function(){
            ws.send("pola6");
        });
		$("#pola6").on('touchstart', function(){
			if(isTouch){
				ws.send("pola6");
			}
		});
		
		$("#pola7").mousedown(function(){
            ws.send("pola7");
        });
		$("#pola7").on('touchstart', function(){
			if(isTouch){
				ws.send("pola7");
			}
		});
		
		//-------intensitas
		$("#l1a").mousedown(function(){
            ws.send("l1a");
        });
		$("#l1a").on('touchstart', function(){
			if(isTouch){
				ws.send("l1a");
			}
		});
		
		$("#l1b").mousedown(function(){
            ws.send("l1b");
        });
		$("#l1b").on('touchstart', function(){
			if(isTouch){
				ws.send("l1b");
			}
		});
		
		$("#l1c").mousedown(function(){
            ws.send("l1c");
        });
		$("#l1c").on('touchstart', function(){
			if(isTouch){
				ws.send("l1c");
			}
		});
		
		$("#l1d").mousedown(function(){
            ws.send("l1d");
        });
		$("#l1d").on('touchstart', function(){
			if(isTouch){
				ws.send("l1d");
			}
		});
		
		
		$("#l2a").mousedown(function(){
            ws.send("l2a");
        });
		$("#l2a").on('touchstart', function(){
			if(isTouch){
				ws.send("l2a");
			}
		});
		
		$("#l2b").mousedown(function(){
            ws.send("l2b");
        });
		$("#l2b").on('touchstart', function(){
			if(isTouch){
				ws.send("l2b");
			}
		});
		
		$("#l2c").mousedown(function(){
            ws.send("l2c");
        });
		$("#l2c").on('touchstart', function(){
			if(isTouch){
				ws.send("l2c");
			}
		});
		
		$("#l2d").mousedown(function(){
            ws.send("l2d");
        });
		$("#l2d").on('touchstart', function(){
			if(isTouch){
				ws.send("l2d");
			}
		});
		
		
		$("#l3a").mousedown(function(){
            ws.send("l3a");
        });
		$("#l3a").on('touchstart', function(){
			if(isTouch){
				ws.send("l3a");
			}
		});
		
		$("#l3b").mousedown(function(){
            ws.send("l3b");
        });
		$("#l3b").on('touchstart', function(){
			if(isTouch){
				ws.send("l3b");
			}
		});
		
		$("#l3c").mousedown(function(){
            ws.send("l3c");
        });
		$("#l3c").on('touchstart', function(){
			if(isTouch){
				ws.send("l3c");
			}
		});
		
		$("#l3d").mousedown(function(){
            ws.send("l3d");
        });
		$("#l3d").on('touchstart', function(){
			if(isTouch){
				ws.send("l3d");
			}
		});
		
		
		$("#l4a").mousedown(function(){
            ws.send("l4a");
        });
		$("#l4a").on('touchstart', function(){
			if(isTouch){
				ws.send("l4a");
			}
		});
		
		$("#l4b").mousedown(function(){
            ws.send("l4b");
        });
		$("#l4b").on('touchstart', function(){
			if(isTouch){
				ws.send("l4b");
			}
		});
		
		$("#l4c").mousedown(function(){
            ws.send("l4c");
        });
		$("#l4c").on('touchstart', function(){
			if(isTouch){
				ws.send("l4c");
			}
		});
		
		$("#l4d").mousedown(function(){
            ws.send("l4d");
        });
		$("#l4d").on('touchstart', function(){
			if(isTouch){
				ws.send("l4d");
			}
		});

      });
