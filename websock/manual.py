import RPi.GPIO as GPIO
import sys
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)

GPIO.output(18, GPIO.HIGH)
GPIO.output(23, GPIO.HIGH)
GPIO.output(24, GPIO.HIGH)
GPIO.output(25, GPIO.HIGH)
GPIO.output(8, GPIO.HIGH)
GPIO.output(7, GPIO.HIGH)
GPIO.output(12, GPIO.HIGH)

arg = sys.argv[1]
if arg=='1':
    GPIO.output(18, GPIO.LOW)
elif arg=='2':
    GPIO.output(23, GPIO.LOW)
elif arg=='3':
    GPIO.output(24, GPIO.LOW)
elif arg=='4':
    GPIO.output(25, GPIO.LOW)
elif arg=='5':
    GPIO.output(8, GPIO.LOW)
elif arg=='6':
    GPIO.output(7, GPIO.LOW)
elif arg=='7':
    GPIO.output(12, GPIO.LOW)
elif arg=='q':
    GPIO.output(18, GPIO.HIGH)
elif arg=='w':
    GPIO.output(23, GPIO.HIGH)
elif arg=='e':
    GPIO.output(24, GPIO.HIGH)
elif arg=='r':
    GPIO.output(25, GPIO.HIGH)
elif arg=='t':
    GPIO.output(8, GPIO.HIGH)
elif arg=='y':
    GPIO.output(7, GPIO.HIGH)
elif arg=='u':
    GPIO.output(12, GPIO.HIGH)