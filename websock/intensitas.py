import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)
pwm = GPIO.PWM(18,900)
pwm2 = GPIO.PWM(23, 900)
pwm3 = GPIO.PWM(24,900)
pwm4 = GPIO.PWM(25, 900)
pwm.start(100)
pwm2.start(100)
pwm3.start(100)
pwm4.start(100)
while True:
	f = open('flag.txt','r')
	data = f.read(1)
	f.close()
	if data=='q':
		pwm.ChangeDutyCycle(0)
	elif data=='w':
		pwm.ChangeDutyCycle(20)
	elif data=='e':
		pwm.ChangeDutyCycle(50)
	elif data=='r':
		pwm.ChangeDutyCycle(100)
	elif data=='a':
		pwm2.ChangeDutyCycle(0)
	elif data=='s':
		pwm2.ChangeDutyCycle(20)
	elif data=='d':
		pwm2.ChangeDutyCycle(50)
	elif data=='f':
		pwm2.ChangeDutyCycle(100)
	elif data=='z':
		pwm3.ChangeDutyCycle(0)
	elif data=='x':
		pwm3.ChangeDutyCycle(20)
	elif data=='c':
		pwm3.ChangeDutyCycle(50)
	elif data=='v':
		pwm3.ChangeDutyCycle(100)
	elif data=='t':
		pwm4.ChangeDutyCycle(0)
	elif data=='y':
		pwm4.ChangeDutyCycle(20)
	elif data=='u':
		pwm4.ChangeDutyCycle(50)
	elif data=='i':
		pwm4.ChangeDutyCycle(100)